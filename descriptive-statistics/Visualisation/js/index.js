/*
5 parameters from current test
5 parameters from nonpatients [avg, std]
*/
visualizeData([3000.5, 800.0, 3500.0, 1000.2, 3000.7], 
              [[3557.387413610061, 857.5641667763227], [588.2960708753399, 67.57071372983616], [3463.66485143799, 411.645945091818], [3458.1791885217, 890.6390810362], [973.4152483217982, 575.0158357938179]]);

// calcParams: [[avg1, stdev1], [avg2,stdev2], ...]

function visualizeData(curParams, calcParams) {
    
    var avgParams = $.map(calcParams, function(i) { return i[0]; });
    var stDevUpper = $.map(calcParams, function(i) { return parseFloat(i[0]) + parseFloat(i[1]); });
    var stDevLower = $.map(calcParams, function(i) { return parseFloat(i[0]) - parseFloat(i[1]); });
    
    var data = {
        labels: ["PARAM1", "PARAM2", "PARAM3", "PARAM4", "PARAM5"], // 5 most informative parameters
        datasets: [ // Chosen exercise set, current test set, standard deviation upper and lower
            {
                label: "CURRENT RESULTS",
                backgroundColor: "rgba(200,200,100,0.2)",
                borderColor: "rgba(200,200,100,1)",
                pointBackgroundColor: "rgba(200,200,100,1)",
                pointBorderColor: "#fff",
                pointHoverBackgroundColor: "#fff",
                pointHoverBorderColor: "rgba(200,200,100,1)",
                data: curParams
            },
            {
                label: "AVERAGE HEALTHY",
                backgroundColor: "rgba(200,200,200,0.2)",
                borderColor: "rgba(200,200,200,1)",
                pointBackgroundColor: "rgba(200,200,200,1)",
                pointBorderColor: "#fff",
                pointHoverBackgroundColor: "#fff",
                pointHoverBorderColor: "rgba(200,200,200,1)",
                data: avgParams
            },
            {
                label: "STANDARD DEVIATION U",
                backgroundColor: "rgba(255,99,132,0)",
                borderColor: "rgba(255,99,132,1)",
                pointBackgroundColor: "rgba(255,99,132,1)",
                pointBorderColor: "#fff",
                pointHoverBackgroundColor: "#fff",
                pointHoverBorderColor: "rgba(255,99,132,1)",
                data: stDevUpper
            },
            {
                label: "STANDARD DEVIATION L",
                backgroundColor: "rgba(255,99,132,0)",
                borderColor: "rgba(255,99,132,1)",
                pointBackgroundColor: "rgba(255,99,132,1)",
                pointBorderColor: "#fff",
                pointHoverBackgroundColor: "#fff",
                pointHoverBorderColor: "rgba(255,99,132,1)",
                data: stDevLower
            }
        ]
    };

    var options = {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero: false,
                    fullWidth: true
                }
                }]
        }
    };

    var ctx = $("#radarChart");
    var myRadarChart = new Chart(ctx, {
        type: 'radar',
        data: data,
        options: options
    }); 
}