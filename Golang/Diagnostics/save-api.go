package main

import (
	"log"
	"net/http"

	"gitlab.com/claantee/ITX8540-backendAPI/Golang/Diagnostics/common"
	"gitlab.com/claantee/ITX8540-backendAPI/Golang/Diagnostics/routers"
)

// Entry point for the program
func main() {

	// Calls startup logic
	common.StartUp()
	// Get the mux router object
	router := routers.InitRoutes()

	server := &http.Server{
		Addr:    common.AppConfig.Server,
		Handler: router,
	}
	log.Println("Listening...")
	server.ListenAndServe()
}
