package data

import (
	"gitlab.com/claantee/ITX8540-backendAPI/Golang/Diagnostics/models"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

type TestRepository struct {
	C *mgo.Collection
}

func (r *TestRepository) Create(test *models.Test) error {
	obj_id := bson.NewObjectId()
	test.Id = obj_id
	err := r.C.Insert(&test)
	return err
}

func (r *TestRepository) GetAll() []models.Test {
	var tests []models.Test
	iter := r.C.Find(nil).Iter()
	result := models.Test{}
	for iter.Next(&result) {
		tests = append(tests, result)
	}
	return tests
}

func (r *TestRepository) GetById(id string) (test models.Test, err error) {
	err = r.C.FindId(bson.ObjectIdHex(id)).One(&test)
	return
}

func (r *TestRepository) Delete(id string) error {
	err := r.C.Remove(bson.M{"_id": bson.ObjectIdHex(id)})
	return err
}
func (r *TestRepository) GetByPatientId(id string) []TestId {
	var tests []TestId
	iter := r.C.Find(bson.M{"patientid": id}).Select(bson.M{"id": 1, "time": 1, "type": 1, "hand": 1, "results": 1}).Iter()
	result := TestId{}
	for iter.Next(&result) {
		tests = append(tests, result)
	}
	return tests
}

type TestId struct {
	Id      bson.ObjectId `bson:"_id,omitempty" json:"id"`
	Time    string        `json:"time"`
	Type    string        `json:"type"`
	Hand    string        `json:"hand"`
	Results []string      `json:"results"`
}
