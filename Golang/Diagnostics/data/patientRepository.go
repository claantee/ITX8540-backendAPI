package data

import (
	"gitlab.com/claantee/ITX8540-backendAPI/Golang/Diagnostics/models"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

type PatientRepository struct {
	C *mgo.Collection
}

func (r *PatientRepository) Create(patient *models.Patient) error {
	err := r.C.Insert(&patient)
	return err
}
func (r *PatientRepository) GetAll() []models.Patient {
	var patients []models.Patient
	iter := r.C.Find(nil).Iter()
	result := models.Patient{}
	for iter.Next(&result) {
		patients = append(patients, result)
	}
	return patients
}
func (r *PatientRepository) GetById(id string) (patient models.Patient, err error) {
	err = r.C.Find(bson.M{"id": id}).One(&patient)
	return
}

func (r *PatientRepository) Update(patient *models.Patient, id string) error {
	//err := r.C.Insert(&patient)
	//return err
	colQuerier := bson.M{"id": id}
	change := bson.M{"$set": bson.M{"dateofbirth": patient.DateOfBirth, "sex": patient.Sex, "hand": patient.Hand}}
	err := r.C.Update(colQuerier, change)
	//if err != nil {
	//	panic(err)
	//}
	return err
}
