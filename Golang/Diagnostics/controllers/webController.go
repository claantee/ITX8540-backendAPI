package controllers

import (
	"net/http"
	"regexp"
)

func StaticHandler(w http.ResponseWriter, r *http.Request) {
	extension, _ := regexp.MatchString("\\.+[a-zA-Z]+", r.URL.EscapedPath())
	// If the url contains an extension, use file server
	if extension {
		http.FileServer(http.Dir("./public/")).ServeHTTP(w, r)
	} else {
		http.ServeFile(w, r, "./public/index.html")
	}
}
