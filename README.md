# Instructions how to deploy Save-API and descriptive-statistics-API using Docker
## Installing to aws ec2 on a amazon linux AMI (other linux machines can be also used)
1. Run following commands:  
 ```sudo yum update -y```  
 ```sudo yum install -y docker```  
 ```sudo service docker start```  
 ```sudo usermod -a -G docker ec2-user```  
 ```logout```  
2. Log back in and run the following  
```sudo yum install -y git```  
```git clone https://gitlab.com/claantee/ITX8540-backendAPI.git backend```  
```sudo -i```  
```curl -L https://github.com/docker/compose/releases/download/1.8.1/docker-compose-`uname -s`-`uname -m` > /usr/local/bin/docker-compose```  
3. Build services with docker compose  
```cd backend```  
```docker-compose up --build -d```  

# MongoDB notes
## Accessing data in mongo container
1. Entering mongodb docker container:  
```docker exec -it diagnostics-db mongo admin```  
2. View all databases  
```show dbs```  
3. Select the database (Test database in this case)  
```use Test```  
4. Viewing collections in database  
```db.getCollectionNames()```  
5. Viewing all of the data in a collection (testdata collection in this case)  
```db.tests.find()```  

## Backup and restore of MongoDB
* Accessing MongoDB container:  
```docker exec -it diagnostics-db /bin/bash```  
* Restoring Data:  
```/backup/restore.sh```  
* Backup Data:  
```/backup/backup.sh```  